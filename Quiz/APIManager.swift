import Foundation

public typealias CompletionClosure = (_ data: Data) -> Void

class APIManager: NSObject{
    
    class func connectToAPI(cClusure: @escaping CompletionClosure){
        let urlString = "https://jsonblob.com/api/jsonBlob/a872004b-adc7-11e7-a347-47f01bdb4eae"
        
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.addValue("xml", forHTTPHeaderField: "Con")
        request.httpBody = Data()
        
        request.httpMethod = "GET"
        let session = Foundation.URLSession.shared
        
        let task = session.dataTask(with: url!) { data, response, error -> Void in
            if let jsonData = data{
                if let jsonString = String(data: jsonData, encoding: .utf8){
                    print(jsonString)
                }
                
                DispatchQueue.main.async {
                    cClusure(jsonData)
                }
            }
                
            else if let requestError = error{
                print("\(requestError) error")
            }
            else{
                print("Unexpected error")
            }
        }
        task.resume()
    }
}
