

import UIKit

protocol ShowAnswer: class {
    func inite(answers: Array<Question>, totalOfScore: Int)
}


class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,ShowAnswer{
    
    var delegate: GoBackDelegate?
    var totalScore: Int = 0
    var listAnswer: Array<Question> = []
    
    @IBOutlet weak var showPoints: UILabel!
    
    func inite(answers: Array<Question>, totalOfScore: Int){
        totalScore = totalOfScore
        listAnswer = answers
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listAnswer.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
     
        cell.question.text = listAnswer[indexPath.row].question
        cell.question.adjustsFontSizeToFitWidth = true
        cell.currentAnswer.text = listAnswer[indexPath.row].currentAnswer
        cell.correctAnswer.text = listAnswer[indexPath.row].correctAnswer
        cell.Color(tORf: listAnswer[indexPath.row].isCorrect)
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let allQuestion: Int = listAnswer.count
        showPoints.text = String(totalScore) + " from " + String(allQuestion)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func restartTab(_ sender: Any) {
        delegate?.goBack()
    }
    
    

}
