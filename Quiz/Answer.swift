
import Foundation

class Answer {
    var question: String
    var answer: String
    var correctAnswer : String
    var isCorrect: Bool
    var questionId: Int
    init(_question: String, _answer: String, _correctAnswer: String, _isCorrect: Bool, _questionId: Int) {
        self.question = _question
        self.answer = _answer
        self.correctAnswer = _correctAnswer
        self.isCorrect = _isCorrect
        self.questionId = _questionId
    }
}
